$(document).ready(function () {

    $("#add_row").on("click", function () {
        // Dynamic Rows Code

        // Get max row id and set new id
        var newid = 0;
        $.each($("#tab_logic tr"), function () {
            if (parseInt($(this).data("id")) > newid) {
                newid = parseInt($(this).data("id"));
            }
        });
        newid++;

        var tr = $("<tr></tr>", {
            id: "item_group" + newid,
            "data-id": newid
        });

        // loop through each td and create new elements with name of newid
        $.each($("#tab_logic tbody tr:nth(0) td"), function () {
            var cur_td = $(this);

            var children = cur_td.children();

            // add new td and element if it has a name
            if ($(this).data("name") != undefined) {
                var td = $("<td></td>", {
                    "data-name": $(cur_td).data("name")
                });

                var c = $(cur_td).find($(children[0]).prop('tagName')).clone();
                c.attr("name", $(cur_td).data("name") + "["+newid+"]");
                c.attr("id", null);
                c.appendTo($(td));
                td.appendTo($(tr));
            } else {
                var td = $("<td></td>", {
                    'text': $('#tab_logic tr').length
                }).appendTo($(tr));
            }
        });

        // add the new row
        $(tr).appendTo($('#tab_logic'));

        $(tr).find("td button.row-remove").on("click", function () {
            $(this).closest("tr").remove();
        });
    });

    $('select#category_id').on('change', function () {
        var val = $(this).val();
        var branch_val = $('select#branch_id').val();
        var $target = $("select#items");
        var $tab_logic = $("#tab_logic");
        var $add_row = $("#add_row");

        if(!val){
            $target.html('');
            $tab_logic.find("button.row-remove:visible").trigger('click');
            $add_row.trigger('click');
            return;
        }

        var jqxhr = $.getJSON("/list/category/" + val + "/items", {ajax: 'true'});

        jqxhr.done(function (resultJSON) {
            var result = resultJSON;
            var options = '';

            $.each(result, function (k, v) {
                options += '<option value="' + k + '">' + v + '</option>';
            });

            $target.html(options);
        });

        jqxhr.fail(function () {
            $target.html('');
        });

        jqxhr.always(function () {
            $tab_logic.find("button.row-remove:visible").trigger('click');
            $add_row.trigger('click');
        });
    });

    $('select#branch_id').on('change', function () {
        var val = $(this).val();
        var $target = $("select#category_id");

        if(!val){
            $target.html('').trigger('change');
            return;
        }

        var jqxhr = $.getJSON("/list/branch/" + val + "/categories", {ajax: 'true'});

        jqxhr.done(function (resultJSON) {
            var result = resultJSON;
            var options = '';

            $.each(result, function (k, v) {
                options += '<option value="' + k + '">' + v + '</option>';
            });

            $target.html(options);
        });

        jqxhr.fail(function () {
            $target.html('');
        });

        jqxhr.always(function () {
            $target.trigger('change');
        });
    });

    $('select#company_id').on('change', function () {
        var val = $(this).val();
        var $target = $("select#branch_id");

        if(!val){
            $target.html('').trigger('change');
            return;
        }

        var jqxhr = $.getJSON("/list/company/" + val + "/branches", {ajax: 'true'});

        jqxhr.done(function (resultJSON) {
            var result = resultJSON;
            var options = '';

            $.each(result, function (k, v) {
                options += '<option value="' + k + '">' + v + '</option>';
            });

            $target.html(options);
        });

        jqxhr.fail(function () {
            $target.html('');
        });

        jqxhr.always(function () {
            $target.trigger('change');
        });

    }).trigger("change");

});
