function initDataTable(){
    // set jQWidgets Theme to "Bootstrap"
    $.jqx.theme = "bootstrap";

    var requirements_source =
    {
        localdata: view.data,
        datatype: "array",
        datafields: [
            {name: 'id', type: 'number'},
            {name: 'company', type: 'string'},
            {name: 'branch', type: 'string'},
            {name: 'category', type: 'string'},
            {name: 'author', type: 'string'},
            {name: 'created_at', type: 'date'}
        ],
        sortcolumn: 'created_at',
        sortdirection: 'desc'
    };

    var requirements_adapter = new $.jqx.dataAdapter(requirements_source);
    var nestedTables = [];

    // create nested DataTable.
    var initRowDetails = function (id, row, element, rowinfo) {
        element.append($("<div style='margin: 10px;'></div>"));
        var nestedDataTable = $(element.children()[0]);
        var items_source = {
            url: "/list/requirement/"+ row.id +"/items?ajax=true",
            datatype: "json",
            dataFields: [
                {name: 'id', type: 'number'},
                {name: 'title', type: 'string' },
                {name: 'quantity', type: 'number'},
                {name: 'unit', type: 'string' }
            ]
        };

        if (nestedDataTable != null) {
            var nestedDataTableAdapter = new $.jqx.dataAdapter(items_source);

            nestedDataTable.jqxDataTable({
                pageable: false,
                source: nestedDataTableAdapter, width: "100%", height: 180,
                columns: [
                    { text: 'Title', dataField: 'title' },
                    { text: 'Quantity', dataField: 'quantity'},
                    { text: 'Unit', dataField: 'unit' }
                ]
            });
            // store the nested Data Tables and use the Requirement ID as a key.
            nestedTables[id] = nestedDataTable;
        }
    };

    $("#dataTable").jqxDataTable({
        width: "100%",
        source: requirements_adapter,
        sortable: true,
        pageable: true,
        altRows: true,
        filterable: true,
        rowDetails: true,
        initRowDetails: initRowDetails,
        ready: function () {},
        columns: [
            {text: 'Company', dataField: 'company' },
            {text: 'Branch', dataField: 'branch'},
            {text: 'Category', dataField: 'category'},
            {text: 'Author', dataField: 'author'},
            {text: 'Created', dataField: 'created_at', cellsFormat: 'd'}
        ]
    });

    $("#pdfExport").jqxButton().click(function () {
        $("#dataTable").jqxDataTable('exportData', 'pdf');
    });
}