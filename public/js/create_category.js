$(document).ready(function() {
    $('select#company').on('change', function(){
        var val = $(this).val();
        $.getJSON("/list/company/" + val +"/branches",{ajax: 'true'}, function(resultJSON){
            var result = resultJSON;
            var options = '';

            $.each(result, function(k, v) {
                options += '<option value="' + k + '">' + v + '</option>';
            });

            $("select#branches\\[\\]").html(options);
        })
    });

    if($("select#company option:selected").length){
        $('select#company').trigger('change');
    }
});
