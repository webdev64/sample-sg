$(document).ready(function () {

    //callback handler for form submit
    $("#search_form").submit(function(e)
    {
        e.preventDefault(); //STOP default action

        var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");
        $.ajax(
            {
                url : formURL,
                type: "GET",
                data : postData,
                success:function(data, textStatus, jqXHR)
                {
                    $("#search_results").html(data);
                    initDataTable();
                },
                error: function(jqXHR, textStatus, errorThrown)
                {

                }
            });

    });

    $('#date_range')
        .val(moment().startOf('month').format('D MMMM, YYYY') + ' - ' + moment().endOf('month').format('D MMMM, YYYY'))
        .daterangepicker({
            "locale": {
                "format": "D MMMM, YYYY"
            },
            "alwaysShowCalendars": true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        });

    $('select#categories\\[\\]').on('change', function () {
        var val = $(this).val();
        var $target = $("select#items\\[\\]");

        if (!val) {
            $target.html('');
            return;
        }

        var jqxhr = $.getJSON("/list/categories/items", {categories: val, ajax: 'true'});

        jqxhr.done(function (resultJSON) {
            var result = resultJSON;
            var options = '';

            $.each(result, function (k, v) {
                options += '<option value="' + k + '">' + v + '</option>';
            });

            $target.html(options);
        });

        jqxhr.fail(function () {
            $target.html('');
        });

        jqxhr.always(function () {
        });
    });

    $('select#branches\\[\\]').on('change', function () {
        var val = $(this).val();
        var $companies = $('select#company');
        var $target = $("select#categories\\[\\]");

        if (!val) {
            if(!$companies.val()){
                $target.html('').trigger('change');
            }
            return;
        }

        var jqxhr = $.getJSON("/list/branches/categories", {branches: val, ajax: 'true'});

        jqxhr.done(function (resultJSON) {
            var result = resultJSON;
            var options = '';

            $.each(result, function (k, v) {
                options += '<option value="' + k + '">' + v + '</option>';
            });

            $target.html(options);
        });

        jqxhr.fail(function () {
            $target.html('');
        });

        jqxhr.always(function () {
            $target.trigger('change');
        });
    });

    $('select#company').on('change', function () {
        var val = $(this).val();

        var targets = [
            {
                target: $("select#branches\\[\\]"),
                url: "/list/company/" + val + "/branches"
            },
            {
                target: $("select#categories\\[\\]"),
                url: "/list/company/" + val + "/categories"
            }
        ];

        $.each(targets, function( index, value ) {
            var $target = $(value.target);
            var url = value.url;

            if (!val) {
                $target.html('').trigger('change');
                return true;
            }

            var jqxhr = $.getJSON(url, {ajax: 'true'});

            jqxhr.done(function (resultJSON) {
                var result = resultJSON;
                var options = '';

                $.each(result, function (k, v) {
                    options += '<option value="' + k + '">' + v + '</option>';
                });

                $target.html(options);
            });

            jqxhr.fail(function () {
                $target.html('');
            });

            jqxhr.always(function () {
                $target.trigger('change');
            });

        });

    }).trigger("change");
});