$(document).ready(function() {
    $('select#company').on('change', function(){
        var val = $(this).val();
        $.getJSON("/list/company/" + val +"/branches",{ajax: 'true'}, function(resultJSON){
            var result = resultJSON;
            var options = '';

            $.each(result, function(k, v) {
                options += '<option value="' + k + '">' + v + '</option>';
            });

            $("select#branches\\[\\]").html(options);
            $("select#categories\\[\\]").html('');
        })
    });

    $('select#branches\\[\\]').on('change', function(){
        var val = $(this).val();
        $.getJSON("/list/branches/categories",{branches: val, ajax: 'true'}, function(resultJSON){
            var result = resultJSON;
            var options = '';

            $.each(result, function(k, v) {
                options += '<option value="' + k + '">' + v + '</option>';
            });

            $("select#categories\\[\\]").html(options);
        })
    });
});
