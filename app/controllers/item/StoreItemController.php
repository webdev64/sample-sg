<?php namespace App\Controllers\Item;

/*
 * Copyright 2016 Filipe Ferreira <work at filipeandre.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use App\Controllers\BaseController;
use App\Repositories\ItemRepository;
use App\Requests\StoreItemRequest;
use Redirect;
use Response;

class StoreItemController extends BaseController
{
    /**
     * The StoreItemRequest instance.
     *
     * @var StoreItemRequest
     */
    protected $request;

    /**
     * The ItemRepository instance.
     *
     * @var ItemRepository
     */
    protected $item_rp;

    /**
     * Assign new objects instances
     *
     * @param StoreItemRequest $request
     * @param ItemRepository $item_rp
     */
    public function __construct(StoreItemRequest $request, ItemRepository $item_rp)
    {
        $this->request = $request;
        $this->item_rp = $item_rp;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $item = $this->item_rp->store($this->request->all());
        $this->item_rp->attachCategories($item->id, $this->request->get('categories'));
        return Redirect::action('App\Controllers\Item\CreateItemController@create')->with('alert', (object) ['class' => 'alert-success', 'message' => 'Item was successfully created.']);
    }
}
