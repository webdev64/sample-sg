<?php namespace App\Controllers\Item;

/*
 * Copyright 2016 Filipe Ferreira <work at filipeandre.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use App\Controllers\BaseController;
use App\Repositories\CategoryRepository;
use App\Repositories\ItemRepository;
use App\Repositories\RequirementRepository;
use Input;
use Response;

class GetItemListAjaxController extends BaseController
{

    /**
     * The CategoryRepository instance.
     *
     * @var CategoryRepository
     */
    protected $categories_rp ;

    /**
     * The ItemRepository instance.
     *
     * @var ItemRepository
     */
    protected $item_rp;

    /**
     * The RequirementRepository instance.
     *
     * @var RequirementRepository
     */
    protected $requirements_rp ;

    /**
     * Assign new objects instances
     *
     * @param CategoryRepository $categories
     * @param ItemRepository $item_rp
     * @param RequirementRepository $requirement_repository
     */
    public function __construct(CategoryRepository $categories, ItemRepository $item_rp, RequirementRepository $requirement_repository)
    {
        $this->categories_rp = $categories;;
        $this->item_rp = $item_rp;
        $this->requirements_rp = $requirement_repository;
    }

    /**
     * Return a Json containing a list of items.
     *
     * @param int $category_id The Category ID
     * @return Response
     */
    public function getCategoryItemList($category_id)
    {
        $items = $this->categories_rp->getItemList($category_id);
        return Response::json($items);
    }

    /**
     * Return a Json containing a list of items by given categories
     *
     * @return Response
     */
    public function getCategoriesItemList()
    {
        $category_ids = Input::get('categories');
        $items = $this->item_rp->getListForCategories($category_ids);
        return Response::json($items);
    }

    /**
     * Return a Json containing a list of items by given requirement
     *
     * @param int $requirement_id The requirement id
     * @return Response
     */
    public function getRequirementItemList($requirement_id)
    {
        $items = $this->requirements_rp->getItemList($requirement_id);
        return Response::json($items);
    }


}
