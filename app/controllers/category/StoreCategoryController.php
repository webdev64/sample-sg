<?php namespace App\Controllers\Category;

/*
 * Copyright 2016 Filipe Ferreira <work at filipeandre.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use App\Controllers\BaseController;
use App\Repositories\CategoryRepository;
use App\Requests\StoreCategoryRequest;
use Redirect;
use Response;

class StoreCategoryController extends BaseController
{
    /**
     * The CategoryRequest instance.
     *
     * @var CategoryRequest
     */
    protected $request;

    /**
     * The CategoryRepository instance.
     *
     * @var CategoryRepository
     */
    protected $category_rp;

    /**
     * Assign new objects instances
     *
     * @param StoreCategoryRequest $request
     * @param CategoryRepository $category_rp
     */
    public function __construct(StoreCategoryRequest $request, CategoryRepository $category_rp)
    {
        $this->request = $request;
        $this->category_rp = $category_rp;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $category = $this->category_rp->store($this->request->all());
        $this->category_rp->attachBranches($category->id, $this->request->get('branches'));
        return Redirect::action('App\Controllers\Category\CreateCategoryController@create')->with('alert', (object) ['class' => 'alert-success', 'message' => 'Category was successfully created.']);
    }
}
