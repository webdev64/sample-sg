<?php namespace App\Controllers\Category;

/*
 * Copyright 2016 Filipe Ferreira <work at filipeandre.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use App\Controllers\BaseController;
use App\Repositories\BranchRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\CompanyRepository;
use Input;
use Response;

class GetCategoryListAjaxController extends BaseController
{

    /**
     * The CategoryRepository instance.
     *
     * @var CategoryRepository
     */
    protected $categories_rp ;

    /**
     * The BranchRepository instance.
     *
     * @var BranchRepository
     */
    protected $branches_rp ;

    /**
     * The CompanyRepository instance.
     *
     * @var CompanyRepository
     */
    protected $companies_rp ;

    /**
     * Assign new objects instances
     *
     * @param CategoryRepository $categories
     * @param BranchRepository $branches
     * @param CompanyRepository $companies_rp
     */
    public function __construct(CategoryRepository $categories, BranchRepository $branches, CompanyRepository $companies_rp)
    {
        $this->categories_rp = $categories;
        $this->branches_rp = $branches;
        $this->companies_rp = $companies_rp;
    }

    /**
     * Return a Json containing a list of categories.
     *
     * @return Response
     */
    public function getBranchesCategoryList()
    {
        $branches_ids = Input::get('branches');
        $categories = $this->categories_rp->getListForBranches($branches_ids);
        return Response::json($categories);
    }

    /**
     * Return a Json containing a list of categories.
     *
     * @param int $branch_id The branch ID
     * @return Response
     */
    public function getBranchCategoryList($branch_id)
    {
        $categories = $this->branches_rp->getCategoryList($branch_id);
        return Response::json($categories);
    }

    /**
     * Return a Json containing a list of categories.
     *
     * @param int $company_id The company ID
     * @return Response
     */
    public function getCompanyCategoryList($company_id)
    {
        $categories = $this->categories_rp->getListForCompany($company_id);
        return Response::json($categories);
    }


}
