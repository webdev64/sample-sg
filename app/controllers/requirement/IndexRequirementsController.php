<?php namespace App\Controllers\Requirement;

/*
 * Copyright 2016 Filipe Ferreira <work at filipeandre.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use App\Controllers\BaseController;
use App\Helpers\DateHelper;
use App\Repositories\RequirementRepository;
use App\Requests\SearchRequirementRequest;
use Response;
use View;

class IndexRequirementsController extends BaseController
{

    /**
     * The SearchRequirementRequest instance.
     *
     * @var SearchRequirementRequest
     */
    protected $request;


    /**
     * The RequirementRepository instance.
     *
     * @var RequirementRepository
     */
    protected $requirements_rp;


    /**
     * Assign new objects instances
     *
     * @param RequirementRepository $requirements_rp
     * @param SearchRequirementRequest $request
     */
    public function __construct(RequirementRepository $requirements_rp,SearchRequirementRequest $request)
    {
        $this->requirements_rp = $requirements_rp;
        $this->request = $request;
    }


    /**
     * Display the listing of requirements
     *
     * @return Response
     */
    public function index()
    {
        $dates =  DateHelper::extract_dates_from_range($this->request->get('date_range'));
        $results = $this->requirements_rp->find($dates->from, $dates->to, $this->request->get('categories'), $this->request->get('items', []));
        return View::make('pages.display_requirements', compact('results'));
    }
}
