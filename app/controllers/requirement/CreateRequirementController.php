<?php namespace App\Controllers\Requirement;

/*
 * Copyright 2016 Filipe Ferreira <work at filipeandre.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use App\Controllers\BaseController;
use App\Repositories\CompanyRepository;
use Response;
use View;

class CreateRequirementController extends BaseController
{

    /**
     * The CompanyRepository instance.
     *
     * @var CompanyRepository
     */
    protected $company_rp;

    /**
     * Assign new objects instances
     *
     * @param CompanyRepository $company_rp
     */
    public function __construct(CompanyRepository $company_rp)
    {
        $this->company_rp = $company_rp;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $companies = $this->company_rp->getList();
        return View::make('pages.create_requirement', compact('companies'));
    }

}
