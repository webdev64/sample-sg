<?php namespace App\Controllers\Requirement;

/*
 * Copyright 2016 Filipe Ferreira <work at filipeandre.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use App\Controllers\BaseController;
use App\Repositories\RequirementRepository;
use App\Requests\StoreRequirementRequest;
use Redirect;
use Response;

class StoreRequirementController extends BaseController
{
    /**
     * The StoreRequirementRequest instance.
     *
     * @var StoreRequirementRequest
     */
    protected $request;

    /**
     * The RequirementRepository instance.
     *
     * @var RequirementRepository
     */
    protected $requirement_rp;

    /**
     * Assign new objects instances
     *
     * @param StoreRequirementRequest $request
     * @param RequirementRepository $requirement_rp
     */
    public function __construct(StoreRequirementRequest $request, RequirementRepository $requirement_rp)
    {
        $this->request = $request;
        $this->requirement_rp = $requirement_rp;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $items = [];
        $quantities = $this->request->get('quantities');

        foreach($this->request->get('items') as $k => $v){
            $items[$v] = ['quantity' => $quantities[$k] ];
        }

        $requirement = $this->requirement_rp->store($this->request->all());
        $this->requirement_rp->attachItems($requirement->id, $items);
        return Redirect::action('App\Controllers\Requirement\CreateRequirementController@create')->with('alert', (object) ['class' => 'alert-success', 'message' => 'Requirement was successfully created.']);
    }
}
