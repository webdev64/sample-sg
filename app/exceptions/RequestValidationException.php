<?php namespace App\Exceptions;

/*
* Copyright 2016 Filipe Ferreira <work at filipeandre.com>.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

use Exception;
use Illuminate\Validation\Validator;
use Input;
use Redirect;
use Response;


class RequestValidationException extends Exception
{
    private $validator;
    private $ajax;
    protected $message;

    /**
     * RequestValidationException constructor.
     *
     * @param Validator $validator
     * @param boolean $ajax
     * @param Exception|null $previous
     */
    public function __construct($validator, $ajax = false, Exception $previous = null)
    {
        $this->validator = $validator;
        $this->ajax= $ajax;
        $this->message = 'Looks like not all fields are valid.';
    }

    /**
     * Return a response with validation errors
     *
     * @return \Illuminate\Http\RedirectResponse |\Illuminate\Http\JsonResponse
     */
    public function getResponse(){
        if($this->ajax) {
            return Response::json(['message'=> $this->message, 'errors' => $this->validator->errors(), 'input'=> Input::all()],422);
        }
        return Redirect::back()->with('errors', $this->validator->errors()->toArray())->withInput(Input::all());
    }
}