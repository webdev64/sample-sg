<?php namespace App\Providers;

use App\Models\Item;
use App\Repositories\ItemRepository;
use App\Requests\StoreItemRequest;
use Illuminate\Support\ServiceProvider;

/*
 * Copyright 2016 Filipe Ferreira <work at filipeandre.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class ItemServiceProvider extends ServiceProvider {

    public function register()
    {
        $this->app->bind('App\Repositories\ItemRepository', function($app)
        {
            return new ItemRepository(new Item());
        });
        $this->app->bind('App\Requests\StoreItemRequest', function($app) {
            $request = new StoreItemRequest();
            $request->copyFrom($app['request']);
            $request->validate();
            return $request;
        });
    }
}