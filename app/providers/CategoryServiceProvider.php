<?php namespace App\Providers;

use App\Models\Category;
use App\Repositories\CategoryRepository;
use App\Requests\StoreCategoryRequest;
use Illuminate\Support\ServiceProvider;

/*
 * Copyright 2016 Filipe Ferreira <work at filipeandre.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class CategoryServiceProvider extends ServiceProvider {

    public function register()
    {
        $this->app->bind('App\Repositories\CategoryRepository', function($app)
        {
            return new CategoryRepository(new Category());
        });
        $this->app->bind('App\Requests\StoreCategoryRequest', function($app) {
            $request = new StoreCategoryRequest();
            $request->copyFrom($app['request']);
            $request->validate();
            return $request;
        });
    }
}