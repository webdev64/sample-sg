<?php namespace App\Requests;

/*
 * Copyright 2016 Filipe Ferreira <work at filipeandre.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class StoreRequirementRequest extends BaseRequest {
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];

        foreach($this->get('items', []) as $k => $v){
            $rules['quantities.'.$k] = 'required|integer|min:1';
        }

        return [
            'author' => 'required|string|min:3',
            'company_id' => 'required',
            'branch_id' => 'required|exists:branches,id',
            'category_id' => 'required|exists:categories,id',
            'comments' => 'string',
            'items' => 'required|array|exists:items,id',
            'quantities' => 'required|array',
        ] + $rules;
    }
}