<?php namespace App\Requests;

use App\Exceptions\RequestValidationException;
use Illuminate\Http\Request;
use Validator;

class BaseRequest extends Request {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * Copy attributes from another request
     *
     * @param Request $obj
     */
    public function  copyFrom($obj)
    {
        $objValues = get_object_vars($obj);
        foreach($objValues AS $key=>$value)
        {
            $this->$key = $value;
        }
    }

    /**
     * Validate the current request params
     *
     * @throws RequestValidationException
     */
    public function validate(){
        $validation = Validator::make($this->all(), $this->rules());
        if ($validation->fails()) {
            throw new RequestValidationException($validation, $this->ajax());
        }
    }
}