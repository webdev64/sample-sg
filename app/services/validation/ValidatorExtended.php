<?php namespace App\Services\Validation;

/*
* Copyright 2016 Filipe Ferreira <work at filipeandre.com>.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

use DateTime;
use Illuminate\Validation\Validator as IlluminateValidator;

class ValidatorExtended extends IlluminateValidator {

    /**
     * The custom messages array
     *
     * @var array
     */
    private $_custom_messages = [
        "date_range" => "The :attribute must follow the pattern: 1 March, 2016 - 30 March, 2016",
    ];

    /**
     * ValidatorExtended constructor.
     * @param \Symfony\Component\Translation\TranslatorInterface $translator
     * @param array $data
     * @param array $rules
     * @param array $messages
     * @param array $customAttributes
     */
    public function __construct( $translator, $data, $rules, $messages = array(), $customAttributes = array() ) {
        parent::__construct( $translator, $data, $rules, $messages, $customAttributes );
        $this->setCustomMessages( $this->_custom_messages );
    }

    /**
     * Validate a date range [1 March, 2016 - 30 March, 2016]
     *
     * @param string $attribute
     * @param mixed $value
     * @param array $parameters (string) $parameters[0] is by default j F, Y, the date format
     * @return bool
     */
    protected function validateDateRange($attribute, $value, $parameters) {

        if(!isset($parameters[0])){
            $parameters[0] = 'j F, Y';
        }

        $dates = explode('-',$value);
        if(count($dates) != 2){ return false; }

        foreach($dates as $date){
            $date = trim($date, ' ');
            $d = DateTime::createFromFormat($parameters[0], $date);
            if($d && $d->format($parameters[0]) == $date) continue;
            return false;
        }

        return true;
    }

}