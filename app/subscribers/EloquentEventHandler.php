<?php namespace App\Subscribers;
/*
* Copyright 2016 Filipe Ferreira <work at filipeandre.com>.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

use Illuminate\Events\Dispatcher;
use Log;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class EloquentEventHandler {

    /**
     * Log Eloquent queries
     */
    public function onEloquentQuery($query, $bindings, $time, $name)
    {
        $view_log = new Logger('Sql');
        $view_log->pushHandler(new StreamHandler(storage_path().'/logs/sql.log', Logger::INFO));

        $data = compact('bindings', 'time', 'name');

        // Format binding data for sql insertion
        foreach ($bindings as $i => $binding)
        {
            if ($binding instanceof \DateTime)
            {
                $bindings[$i] = $binding->format('\'Y-m-d H:i:s\'');
            }
            else if (is_string($binding))
            {
                $bindings[$i] = "'$binding'";
            }
        }

        // Insert bindings into query
        $query = str_replace(array('%', '?'), array('%%', '%s'), $query);
        $query = vsprintf($query, $bindings);

        $view_log->addInfo($query, $data);
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Dispatcher  $events
     * @return array
     */
    public function subscribe($events)
    {
        $events->listen('illuminate.query', 'App\Subscribers\EloquentEventHandler@onEloquentQuery');
    }

}