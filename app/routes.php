<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//-----------------------------------------------------
// SB Admin 2
//-----------------------------------------------------

Route::get('/', function () {
    return View::make('index');
});

Route::get('/pages/blank', function () {
    return View::make('pages.blank');
});
Route::get('/pages/buttons', function () {
    return View::make('pages.buttons');
});
Route::get('/pages/flot', function () {
    return View::make('pages.flot');
});
Route::get('/pages/forms', function () {
    return View::make('pages.forms');
});
Route::get('/pages/grid', function () {
    return View::make('pages.grid');
});
Route::get('/pages/icons', function () {
    return View::make('pages.icons');
});
Route::get('/pages/index', function () {
    return View::make('pages.index');
});
Route::get('/pages/login', function () {
    return View::make('pages.login');
});
Route::get('/pages/morris', function () {
    return View::make('pages.morris');
});
Route::get('/pages/notifications', function () {
    return View::make('pages.notifications');
});
Route::get('/pages/panels-wells', function () {
    return View::make('pages.panels-wells');
});
Route::get('/pages/tables', function () {
    return View::make('pages.tables');
});
Route::get('/pages/typography', function () {
    return View::make('pages.typography');
});

//-----------------------------------------------------
// Smartdigit
//-----------------------------------------------------

//Branches
Route::get('/list/company/{id}/branches', ['uses' => 'App\Controllers\Branch\GetBranchListAjaxController@getCompanyBranchList']);

//Category
Route::get('/pages/create_category', ['uses' => 'App\Controllers\Category\CreateCategoryController@create']);
Route::post('/pages/create_category', ['before'=> 'csrf', 'uses' => 'App\Controllers\Category\StoreCategoryController@store']);
Route::get('/list/branches/categories', ['uses' => 'App\Controllers\Category\GetCategoryListAjaxController@getBranchesCategoryList']);
Route::get('/list/branch/{id}/categories', ['uses' => 'App\Controllers\Category\GetCategoryListAjaxController@getBranchCategoryList']);
Route::get('/list/company/{id}/categories', ['uses' => 'App\Controllers\Category\GetCategoryListAjaxController@getCompanyCategoryList']);

//Items
Route::get('/pages/create_items', ['uses' => 'App\Controllers\Item\CreateItemController@create']);
Route::post('/pages/create_items', ['before'=> 'csrf', 'uses' => 'App\Controllers\Item\StoreItemController@store']);
Route::get('/list/category/{id}/items', ['uses' => 'App\Controllers\Item\GetItemListAjaxController@getCategoryItemList']);
Route::get('/list/categories/items', ['uses' => 'App\Controllers\Item\GetItemListAjaxController@getCategoriesItemList']);
Route::get('/list/requirement/{id}/items', ['uses' => 'App\Controllers\Item\GetItemListAjaxController@getRequirementItemList']);

//Requirements
Route::get('/pages/create_requirement', ['uses' => 'App\Controllers\Requirement\CreateRequirementController@create']);
Route::post('/pages/create_requirement', ['before'=> 'csrf', 'uses' => 'App\Controllers\Requirement\StoreRequirementController@store']);
Route::get('/pages/search_requirements', ['uses' => 'App\Controllers\Requirement\SearchRequirementsController@index']);
Route::get('/pages/list_requirements', ['uses' => 'App\Controllers\Requirement\IndexRequirementsController@index']);