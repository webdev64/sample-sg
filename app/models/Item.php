<?php namespace App\Models;

use Eloquent;

class Item extends Eloquent
{
    protected $fillable = ['title', 'author', 'unit'];
    protected $hidden = ['pivot'];

    public function categories()
    {
        return $this->belongsToMany('App\Models\Category', 'item_categories');
    }

    public function requirements()
    {
        return $this->belongsToMany('App\Models\Requirement', 'requirement_items');
    }
}