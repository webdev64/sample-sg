<?php namespace App\Models;

use Eloquent;

class Category extends Eloquent
{
    protected $fillable = ['title', 'description', 'author'];

    public function branches()
    {
        return $this->belongsToMany('App\Models\Branch', 'branch_categories');
    }

    public function items()
    {
        return $this->belongsToMany('App\Models\Item', 'item_categories');
    }

    public function requirements()
    {
        return $this->hasMany('App\Models\Requirement');
    }
}