<?php namespace App\Models;

use Eloquent;

class Branch extends Eloquent {
	protected $fillable = ['company_id', 'name'];

	public function categories()
	{
		return $this->belongsToMany('App\Models\Category', 'branch_categories');
	}

	public function post()
	{
		return $this->belongsTo('App\Models\Company');
	}

	public function requirements()
	{
		return $this->hasMany('App\Models\Requirement');
	}
}