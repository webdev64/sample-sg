<?php namespace App\Models;

use Eloquent;

class Company extends Eloquent
{
    protected $fillable = ['company_group_id', 'name'];

    public function branches()
    {
        return $this->hasMany('App\Models\Branch');
    }
}