<?php namespace App\Models;

use Eloquent;

class Requirement extends Eloquent
{
    protected $fillable = ['branch_id', 'category_id', 'author', 'comments'];
    protected $hidden = ['pivot'];

    public function items()
    {
        return $this->belongsToMany('App\Models\Item', 'requirement_items');
    }

    public function category(){
        return $this->belongsTo('App\Models\Category');
    }

    public function branch(){
        return $this->belongsTo('App\Models\Branch');
    }
}