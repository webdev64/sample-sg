@extends('pages.partials.master')
@section('styles')
@stop
@section('main')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Smartdigit</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                {{-- The Content --}}
                <div class="col-md-12 col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Search Requirements
                        </div>
                        <div class="panel-body">
                            @if(Session::has('alert'))
                                <div class="alert {{Session::get('alert')->class}}" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    {{Session::get('alert')->message}}
                                </div>
                            @endif
                            {{ Form::open(["id" => "search_form", "role"=> "form", 'action' => 'App\Controllers\Requirement\IndexRequirementsController@index', 'method' => 'get']) }}

                            <div class="form-group @if(array_has(Session::get('errors'), 'company')) has-error @endif">
                                {{ Form::label('company', 'Company *') }}
                                {{ Form::select('company', $companies, null, ['required', 'size' => 4, 'class' => 'form-control'])}}
                                @if(array_has(Session::get('errors'), 'company'))<p
                                        class="help-block">{{ Session::get('errors')['company'][0] }}</p> @endif
                            </div>

                            <div class="form-group @if(array_has(Session::get('errors'), 'branches')) has-error @endif">
                                {{ Form::label('branches[]', 'Branches') }}
                                {{ Form::select('branches[]', [], null, ['multiple', 'class' => 'form-control'])}}
                                @if(array_has(Session::get('errors'), 'branches'))<p
                                        class="help-block">{{ Session::get('errors')['branches'][0] }}</p> @endif
                            </div>
                            <div class="form-group @if(array_has(Session::get('errors'), 'categories')) has-error @endif">
                                {{ Form::label('categories[]', 'Categories *') }}
                                {{ Form::select('categories[]', [], null, ['required','multiple', 'class' => 'form-control'])}}
                                @if(array_has(Session::get('errors'), 'categories'))<p
                                        class="help-block">{{ Session::get('errors')['categories'][0] }}</p> @endif
                            </div>
                            <div class="form-group @if(array_has(Session::get('errors'), 'items')) has-error @endif">
                                {{ Form::label('items[]', 'Items') }}
                                {{ Form::select('items[]', [], null, ['multiple', 'class' => 'form-control'])}}
                                @if(array_has(Session::get('errors'), 'items'))<p
                                        class="help-block">{{ Session::get('errors')['items'][0] }}</p> @endif
                            </div>
                            <div class="form-group @if(array_has(Session::get('errors'), 'date_range')) has-error @endif">
                                {{ Form::label('date_range', 'Date range *') }}
                                {{ Form::text('date_range', null, ['required','class' => 'form-control']) }}
                                @if(array_has(Session::get('errors'), 'date_range'))<p
                                        class="help-block">{{ Session::get('errors')['date_range'][0] }}</p> @endif
                            </div>
                            {{Form::submit('Find', ['class' => 'btn btn-default'])}}
                            {{Form::reset('Reset Form', ['class' => 'btn btn-default'])}}
                            {{Form::close()}}

                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-8" id="search_results">
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
@stop
@section('scripts')
    <script src="../js/search_requirements.js"></script>
@stop
