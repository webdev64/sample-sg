@extends('pages.partials.master')
@section('styles')
@stop
@section('main')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Smartdigit</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                {{-- The Content --}}
                <div class="col-md-12 col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Create requirement
                        </div>
                        <div class="panel-body">

                            @if(Session::has('alert'))
                                <div class="alert {{Session::get('alert')->class}}" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    {{Session::get('alert')->message}}
                                </div>
                            @endif
                            {{ Form::open(["role"=> "form", 'action' => 'App\Controllers\Requirement\StoreRequirementController@store', 'method' => 'post']) }}
                            <div class="form-group @if(array_has(Session::get('errors'), 'author')) has-error @endif">
                                {{ Form::label('author', 'Author *') }}
                                {{ Form::text('author', null, ['required', 'class' => 'form-control', 'placeholder' => 'John Doe']) }}
                                @if(array_has(Session::get('errors'), 'author'))<p
                                        class="help-block">{{ Session::get('errors')['author'][0] }}</p> @endif
                            </div>
                            <div class="form-group @if(array_has(Session::get('errors'), 'company_id')) has-error @endif">
                                {{ Form::label('company_id', 'Company *') }}
                                {{ Form::select('company_id', $companies, null, ['required', 'class' => 'form-control'])}}
                                @if(array_has(Session::get('errors'), 'company_id'))<p
                                        class="help-block">{{ Session::get('errors')['company_id'][0] }}</p> @endif
                            </div>
                            <div class="form-group @if(array_has(Session::get('errors'), 'branch_id')) has-error @endif">
                                {{ Form::label('branch_id', 'Branch *') }}
                                {{ Form::select('branch_id', [], null, ['required', 'class' => 'form-control'])}}
                                @if(array_has(Session::get('errors'), 'branch_id'))<p
                                        class="help-block">{{ Session::get('errors')['branch_id'][0] }}</p> @endif
                            </div>
                            <div class="form-group @if(array_has(Session::get('errors'), 'category_id')) has-error @endif">
                                {{ Form::label('category_id', 'Category *') }}
                                {{ Form::select('category_id', [], null, ['required', 'class' => 'form-control'])}}
                                @if(array_has(Session::get('errors'), 'category_id'))<p
                                        class="help-block">{{ Session::get('errors')['category_id'][0] }}</p> @endif
                            </div>
                            <div class="form-group @if(array_has(Session::get('errors'), 'items')) has-error @endif">
                                {{ Form::label('items', 'Items *') }}


                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover" id="tab_logic">
                                        <thead>
                                        <tr>
                                            <th class="text-center">
                                                Item
                                            </th>
                                            <th class="text-center">
                                                Quantity
                                            </th>
                                            <th class="text-center">
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr id='item_group0' data-id="0" class="hidden">
                                            <td data-name="items">
                                                {{ Form::select(null, [], null, ['id' => "items", 'required', 'class' => 'form-control'])}}
                                            </td>
                                            <td data-name="quantities">
                                                {{ Form::number(null, '1', ["id" => "quantities", "min"=>"1", "step"=>"1", 'required', 'class' => 'form-control']) }}
                                            </td>
                                            <td data-name="del">
                                                <button name="del"
                                                        class='btn btn-danger glyphicon glyphicon-remove row-remove'></button>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                @if(array_has(Session::get('errors'), 'items'))<p
                                        class="help-block">{{ Session::get('errors')['items'][0] }}</p> @endif
                                <div class="text-right">
                                    <a id="add_row" class="btn btn-default">Add More Items</a>
                                </div>
                            </div>
                            <div class="form-group @if(array_has(Session::get('errors'), 'comments')) has-error @endif">
                                {{ Form::label('comments', 'Comments') }}
                                {{ Form::textarea('comments', null, ['class' => 'form-control', 'placeholder' => 'This is my comment...', 'rows' => '3']) }}
                                @if(array_has(Session::get('errors'), 'comments'))<p
                                        class="help-block">{{ Session::get('errors')['comments'][0] }}</p> @endif
                            </div>
                            {{Form::submit('Submit Button', ['class' => 'btn btn-default'])}}
                            {{Form::reset('Reset Button', ['class' => 'btn btn-default'])}}
                            {{Form::close()}}
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
@stop
@section('scripts')
    <script src="../js/create_requirement.js"></script>
@stop
