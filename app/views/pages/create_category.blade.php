@extends('pages.partials.master')
@section('styles')
@stop
@section('main')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Smartdigit</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                {{-- The Content --}}
                <div class="col-md-12 col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Create category
                        </div>
                        <div class="panel-body">

                            @if(Session::has('alert'))
                                <div class="alert {{Session::get('alert')->class}}" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    {{Session::get('alert')->message}}
                                </div>
                            @endif
                            {{ Form::open(["role"=> "form", 'action' => 'App\Controllers\Category\StoreCategoryController@store', 'method' => 'post']) }}
                            <div class="form-group @if(array_has(Session::get('errors'), 'title')) has-error @endif">
                                {{ Form::label('title', 'Title *') }}
                                {{ Form::text('title', null, ['required', 'class' => 'form-control', 'placeholder' => 'Category 1']) }}
                                @if(array_has(Session::get('errors'), 'title'))<p
                                        class="help-block">{{ Session::get('errors')['title'][0] }}</p> @endif
                            </div>
                            <div class="form-group @if(array_has(Session::get('errors'), 'author')) has-error @endif">
                                {{ Form::label('author', 'Author *') }}
                                {{ Form::text('author', null, ['required', 'class' => 'form-control', 'placeholder' => 'John Doe']) }}
                                @if(array_has(Session::get('errors'), 'author'))<p
                                        class="help-block">{{ Session::get('errors')['author'][0] }}</p> @endif
                            </div>
                            <div class="form-group @if(array_has(Session::get('errors'), 'company')) has-error @endif">
                                {{ Form::label('company', 'Company *') }}
                                {{ Form::select('company', $companies, null, ['required', 'size' => 4, 'class' => 'form-control'])}}
                                @if(array_has(Session::get('errors'), 'company'))<p
                                        class="help-block">{{ Session::get('errors')['company'][0] }}</p> @endif
                            </div>
                            <div class="form-group @if(array_has(Session::get('errors'), 'branches')) has-error @endif">
                                {{ Form::label('branches[]', 'Branches *') }}
                                {{ Form::select('branches[]', [], null, ['required', 'multiple', 'class' => 'form-control'])}}
                                @if(array_has(Session::get('errors'), 'branches'))<p
                                        class="help-block">{{ Session::get('errors')['branches'][0] }}</p> @endif
                            </div>
                            <div class="form-group @if(array_has(Session::get('errors'), 'description')) has-error @endif">
                                {{ Form::label('description', 'Description') }}
                                {{ Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'This category is amazing because is the first...', 'rows' => '3']) }}
                                @if(array_has(Session::get('errors'), 'description'))<p
                                        class="help-block">{{ Session::get('errors')['description'][0] }}</p> @endif
                            </div>
                            {{Form::submit('Submit Button', ['class' => 'btn btn-default'])}}
                            {{Form::reset('Reset Button', ['class' => 'btn btn-default'])}}
                            {{Form::close()}}
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
@stop
@section('scripts')
    <script src="../js/create_category.js"></script>
@stop
