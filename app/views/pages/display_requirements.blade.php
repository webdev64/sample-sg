<div class="panel panel-default">
    <div class="panel-heading">
        Results
    </div>
    <div class="panel-body">
        <div id="employee" style="width: 100%; height: 100%;" class="pane">
            <div class="text-left">
                <input type="button" value="Export to PDF" id="pdfExport" role="button"
                       class="jqx-rc-all jqx-button jqx-widget jqx-fill-state-normal"
                       aria-disabled="false">
            </div>
            <br>
            <div style="border: none;" id='dataTable'></div>
        </div>
    </div>
</div>


<script>
    var view = {
        data: JSON.parse('{{$results->toJson()}}')
    };
</script>
<script src="../js/display_requirements.js"></script>