<?php namespace App\Repositories;
use App\Models\Branch;

/*
 * Copyright 2016 Filipe Ferreira <work at filipeandre.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class BranchRepository
{
    /**
     * The Branch instance.
     *
     * @var Branch
     */
    protected $model;

    /**
     * Create a new BranchRepository instance.
     *
     * @param Branch $branch
     */
    public function __construct(Branch $branch)
    {
        $this->model = $branch;
    }

    /**
     * List categories from a branch
     *
     * @param int $branch_id The branch Id
     * @return array
     */
    public function getCategoryList($branch_id)
    {
        return $this->model->findOrFail($branch_id)->categories()->getQuery()->lists('title','id');
    }
}