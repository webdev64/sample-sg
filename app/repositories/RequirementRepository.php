<?php namespace App\Repositories;
use App\Models\Requirement;
use DateTime;

/*
 * Copyright 2016 Filipe Ferreira <work at filipeandre.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class RequirementRepository
{
    /**
     * The Requirement instance.
     *
     * @var Requirement
     */
    protected $model;

    /**
     * Create a new RequirementRepository instance.
     *
     * @param Requirement $requirement
     */
    public function __construct(Requirement $requirement)
    {
        $this->model = $requirement;
    }

    /**
     * Create requirement.
     *
     * @param  array  $attributes
     * @return Requirement
     */
    public function store($attributes)
    {
        return $this->model->create($attributes);
    }

    /**
     * Add items to a requirement
     *
     * @param int $id The requirement Id
     * @param array $items The array with items [$id => ['quantity' => 2]
     */
    public function attachItems($id, array $items){
        $this->model->findOrFail($id)->items()->attach($items);
    }

    /**
     * Get date to populate a grid with of requirements
     *
     * @param DateTime $from
     * @param DateTime $to
     * @param array $categories
     * @param array $items
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function find(DateTime $from, DateTime $to, array $categories, array $items){
        $query = $this->model
            ->where('requirements.created_at', '>=', $from)
            ->where('requirements.created_at','<=', $to)
            ->whereIn('category_id',$categories)
            ->leftJoin('branches','requirements.branch_id', '=', 'branches.id' )
            ->leftJoin('companies','branches.company_id', '=', 'companies.id' )
            ->leftJoin('categories','requirements.category_id', '=', 'categories.id' );

        if($items != []){
            $query = $query->leftJoin('requirement_items','requirements.id', '=', 'requirement_items.requirement_id')->whereIn('requirement_items.item_id',$items);
        }
        return $query->get(['requirements.id', 'companies.name as company', 'branches.name as branch', 'categories.title as category', 'requirements.author', 'requirements.created_at']);
    }

    /**
     * List items from a requirement
     *
     * @param int $requirement_id The requirement id
     * @return array
     */
    public function getItemList($requirement_id)
    {
        $result =  $this->model->findOrFail($requirement_id)->items()->get(['id', 'title', 'unit', 'quantity']);
        return $result;
    }

}