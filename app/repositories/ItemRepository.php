<?php namespace App\Repositories;
use App\Models\Category;
use App\Models\Item;

/*
 * Copyright 2016 Filipe Ferreira <work at filipeandre.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class ItemRepository
{
    /**
     * The Item instance.
     *
     * @var Item
     */
    protected $model;

    /**
     * Create a new ItemRepository instance.
     *
     * @param Item $item
     */
    public function __construct(Item $item)
    {
        $this->model = $item;
    }

    /**
     * Create item.
     *
     * @param  array  $attributes
     * @return Item
     */
    public function store($attributes)
    {
        return $this->model->create($attributes);
    }

    /**
     * Add categories to a item
     *
     * @param int $id The item Id
     * @param array $categories_ids The categories ids
     */
    public function attachCategories($id, $categories_ids){
        $this->model->findOrFail($id)->categories()->attach($categories_ids);
    }

    /**
     * Get items owned by multiple categories
     *
     * @param  array  $category_ids
     * @return array
     */
    public function getListForCategories($category_ids){
        $result = $this->model->leftJoin('item_categories', 'items.id', '=', 'item_categories.item_id')->whereIn('item_categories.category_id',$category_ids)->get(['items.id', 'items.title']);
        $return = [];

        foreach($result as $item){
            $return[$item->id] = $item->title;
        }

        return $return;
    }

}