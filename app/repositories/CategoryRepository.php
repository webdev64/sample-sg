<?php namespace App\Repositories;
use App\Models\Category;

/*
 * Copyright 2016 Filipe Ferreira <work at filipeandre.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class CategoryRepository
{
    /**
     * The Category instance.
     *
     * @var Category
     */
    protected $model;

    /**
     * Create a new CategoryRepository instance.
     *
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        $this->model = $category;
    }

    /**
     * Create category.
     *
     * @param  array  $attributes
     * @return Category
     */
    public function store($attributes)
    {
        return $this->model->create($attributes);
    }

    /**
     * Add branches to a category
     *
     * @param int $id The category Id
     * @param array $branch_ids The branch ids
     */
    public function attachBranches($id, $branch_ids){
        $this->model->find($id)->branches()->attach($branch_ids);
    }

    /**
     * Get categories owned by multiple branches
     *
     * @param  array  $branches_ids
     * @return array
     */
    public function getListForBranches($branches_ids)
    {
        $result = $this->model->leftJoin('branch_categories', 'categories.id', '=', 'branch_categories.category_id')->whereIn('branch_categories.branch_id',$branches_ids)->get(['categories.id', 'categories.title']);
        $return = [];

        foreach($result as $item){
            $return[$item->id] = $item->title;
        }

        return $return;
    }

    /**
     * Get categories owned by a company
     *
     * @param  int  $company_id
     * @return array
     */
    public function getListForCompany($company_id)
    {
        $result = $this->model
            ->leftJoin('branch_categories', 'categories.id', '=', 'branch_categories.category_id')
            ->leftJoin('branches', 'branches.id', '=', 'branch_categories.branch_id')
            ->where('branches.company_id','=', $company_id)
            ->get(['categories.id', 'categories.title']);

        $return = [];

        foreach($result as $item){
            $return[$item->id] = $item->title;
        }

        return $return;
    }

    /**
     * List items from a category
     *
     * @param int $category_id The category Id
     * @return array
     */
    public function getItemList($category_id)
    {
        $result =  $this->model->findOrFail($category_id)->items()->get(['title','id', 'unit']);
        $return = [];

        foreach($result as $item){
            $return[$item->id] = $item->title . ' ('. $item->unit . ') ';
        }

        return $return;
    }
    
}