<?php namespace App\Repositories;

use App\Models\Company;

/*
 * Copyright 2016 Filipe Ferreira <work at filipeandre.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class CompanyRepository
{

    /**
     * The Company instance.
     *
     * @var Company
     */
    protected $model;

    /**
     * Create a new Company instance.
     *
     * @param Company $company
     */
    public function __construct(Company $company)
    {
        $this->model = $company;
    }

    /**
     * List companies
     *
     * @return array
     */
    public function getList()
    {
        return $this->model->lists('name', 'id');
    }

    /**
     * List branch from a company
     *
     * @param int $company_id The company Id
     * @return array
     */
    public function getBranchesList($company_id)
    {
        return $this->model->findOrFail($company_id)->branches()->getQuery()->lists('name','id');
    }

}