<?php namespace App\Helpers;
/*
* Copyright 2016 Filipe Ferreira <work at filipeandre.com>.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

use DateTime;

class DateHelper
{
    /**
     * Extract $from $to dates from $range
     *
     * @param string $range The string containing the dates [1 March, 2016 - 30 March, 2016]
     * @param string $format, by default is [j F, Y]
     * @return object (DateTime)$obj->from (DateTime)$obj->to
     */
    public static function extract_dates_from_range($range, $format = 'j F, Y'){
        $dates = explode('-',$range);
        $from = DateTime::createFromFormat($format, trim($dates[0], ' '))->setTime(0, 0, 0);
        $to = DateTime::createFromFormat($format, trim($dates[1], ' '))->setTime(0, 0, 0);
        return (object) compact('from', 'to');
    }
}