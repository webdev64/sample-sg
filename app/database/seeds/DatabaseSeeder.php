<?php

class DatabaseSeeder extends Seeder
{

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('CompanyTableSeeder');
		$this->command->info('companies table seeded!');

		$this->call('BranchTableSeeder');
		$this->command->info('branch table seeded!');

		$this->call('CategoryTableSeeder');
		$this->command->info('categories table seeded!');

		$this->call('BranchCategoryTableSeeder');
		$this->command->info('branch_categories table seeded!');
	}
}
