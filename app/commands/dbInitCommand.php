<?php

namespace App\Commands;

use App\Services\SqlMigrations;
use Illuminate\Console\Command;

class dbInitCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'db:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a migration from existing database';

    /**
     * Create a new command instance.
     *
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire() {
        if (!$this->confirm("Are you sure you want to create a migration from the current database? (Yes / No)", false)) {
            $this->comment('No actions performed.');
            return;
        }
        $migrate = new SqlMigrations();
        $migrate->convert('smartdigit');
        $migrate->write();
        $this->info('Migration created!');
    }

}
